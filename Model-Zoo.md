# The KISD model zoo

<br/>

Link to the [KISD model zoo Folder](https://th-koeln.sciebo.de/s/lWo6dhDtfQHCmBA) on SCIEBO

<br/>

[![](/thumbnail/cat.jpg)](#L161) | ![](/thumbnail/dog.jpg) | ![](/thumbnail/afhqwild.jpg)(https://git.rwth-aachen.de/-/ide/project/laura_juliane.wagner/the-kisd-model-zoo/edit/main/-/Model-Zoo.md#L24) |![](/thumbnail/brecahad.jpg) | ![](/thumbnail/cakes.jpg) | ![](/thumbnail/celebrities.png) | ![](/thumbnail/stylegan256_slim.jpg) | ![](/thumbnail/chair.jpg) | ![](/thumbnail/ffhq-512-avg-tpurun1.jpg) | ![](/thumbnail/ffhq1024.jpg) | ![](/thumbnail/floorplans.jpg) | ![](/thumbnail/flower.jpg) | ![](/thumbnail/freagan.jpg) | ![](/thumbnail/fursona.jpg) | ![](/thumbnail/stylegan2-horse-config-f.jpg) | ![](/thumbnail/ImageNet.jpg) | ![](/thumbnail/insect.png) | ![](/thumbnail/karrl.jpg) | ![](/thumbnail/mapdreamer.jpg) | ![](/thumbnail/metfaces.jpg) | ![](/thumbnail/microscope.jpg) | ![](/thumbnail/modernart.jpg) | ![](/thumbnail/ponies.jpg) | ![](/thumbnail/pokemon.jpg) | ![](/thumbnail/stylegan2-car-config-e.jpg) | ![](/thumbnail/stylegan2-cat-config-f256.jpg) | ![](/thumbnail/stylegan2-church-config-f256.jpg) | ![](/thumbnail/frame0147.jpg) | ![](/thumbnail/wildlife.jpg) | ![](/thumbnail/tryptophobia1024.jpg)
| ![](/thumbnail/frame0148.jpg) | ![](/thumbnail/insect.png) | ![](/thumbnail/anime.jpg)
<br/>




# afhqcat
| ![](/gif/afhqcat.gif) | resolution: 512x512 <br/> <br/> author: [Yunjey Choi*](https://github.com/yunjey), Youngjung Uh*, Jaejun Yoo*, [Jung-Woo Ha](https://www.facebook.com/jungwoo.ha.921) <br/> <br/> dataset: [AFHQ](https://github.com/clovaai/stargan-v2/blob/master/README.md#animal-faces-hq-dataset-afhq) <br/> <br/> license: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) <br/> <br/>  misc.: bla |
| ------ | ------ |

# afhqdog
| ![](/gif/afhqdog.gif) | resolution: 512x512 <br/> <br/> author: [Yunjey Choi*](https://github.com/yunjey), Youngjung Uh*, Jaejun Yoo*, [Jung-Woo Ha](https://www.facebook.com/jungwoo.ha.921) <br/> <br/> dataset: [AFHQ](https://github.com/clovaai/stargan-v2/blob/master/README.md#animal-faces-hq-dataset-afhq) <br/> <br/> license: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) <br/> <br/>  misc.: bla |
| ------ | ------ |

# afhqwild
| ![](/gif/afhqwild.gif) | resolution: 512x512 <br/> <br/> author: [Yunjey Choi*](https://github.com/yunjey), Youngjung Uh*, Jaejun Yoo*, [Jung-Woo Ha](https://www.facebook.com/jungwoo.ha.921) <br/> <br/> dataset: [AFHQ](https://github.com/clovaai/stargan-v2/blob/master/README.md#animal-faces-hq-dataset-afhq) <br/> <br/> license: [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) <br/> <br/>  misc.: bla |
| ------ | ------ |

# brecahad
| ![](/gif/brecahad.gif) | resolution: 256x256 <br/> <br/> author: ... <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |


# cakes
| ![](/gif/cakes.gif) | resolution: 256x256 <br/> <br/> author: ... <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |


# celebrity_slim
| ![](/gif/celebrity_slim.gif) | resolution: 256x256 <br/> <br/> author: ... <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# ffhq256_slim
| ![](/gif/ffhq256.gif) | resolution: 256x256 <br/> <br/> author:  [NVLabs] (https://www.nvidia.com/en-us/research/) <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# chair
| ![](/gif/chair.gif) | resolution: 256x256 <br/> <br/> author: L.Wagner <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# ffhq-512-avg-tpurun1
| ![](/gif/ffhq-512-avg-tpurun1.gif) | resolution: 256x256 <br/> <br/> author: [NVLabs] (https://www.nvidia.com/en-us/research/) <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |


# ffhq1024
| ![](/gif/ffhq1024.gif) | resolution: 256x256 <br/> <br/> author: [NVLabs] (https://www.nvidia.com/en-us/research/) <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# floorplans
| ![](/gif/floorplans.gif) | resolution: 256x256 <br/> <br/> author: <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |


# flowers
| ![](/gif/flowers_02.gif) | resolution: 256x256 <br/> <br/> author:  <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# freaGAN
| ![](/gif/Freagan.gif) | resolution: 512x512 <br/> <br/> author:  <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# fursona
| ![](/gif/fursona.gif) | resolution: 512x512 <br/> <br/> author:  <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# horse
| ![](/gif/horse.gif) | resolution: 256x256 <br/> <br/> author: [NVLabs] (https://www.nvidia.com/en-us/research/) <br/> <br/> dataset: .... <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# ImageNet
| ![](/gif/imagenet.gif) | resolution: 256x256 <br/> <br/> author: [NVLabs] (https://www.nvidia.com/en-us/research/) <br/> <br/> dataset: ImageNet <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# insects
| ![](/gif/insects.gif) | resolution: 256x256 <br/> <br/> author: L.Wagner, L.Mantel <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# Long_Krrrl_Drawings
| ![](/gif/Long_Krrrl_Drawings.gif) | resolution: 256x256 <br/> <br/> author:  <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# mapdreamer
| ![](/gif/mapdreamer.gif) | resolution: 1024x1024 <br/> <br/> author:  <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# metfaces
| ![](/gif/metfaces256.gif) | resolution: 1024x1024 <br/> <br/> author: [NVLabs] (https://www.nvidia.com/en-us/research/) <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# microscope
| ![](/gif/microscope.gif) | resolution: 1024x1024 <br/> <br/> author:  <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# modernart
| ![](/gif/modernart.gif) | resolution: 1024x1024 <br/> <br/> author:  <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# ponies
| ![](/gif/network-ponies-1024-151552.gif) | resolution: 1024x1024 <br/> <br/> author:  <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# pokemon
| ![](/gif/pokemon.gif) | resolution: 256x256 <br/> <br/> author:  <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# stylegan2-car-config-e
| ![](/gif/stylegan2-car-config-e.gif) | resolution: 256x256 <br/> <br/> author:  <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# stylegan2-cat-config-f256
| ![](/gif/stylegan2-cat-config-f256.gif) | resolution: 256x256 <br/> <br/> author: [NVLabs] (https://www.nvidia.com/en-us/research/) <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# stylegan2-church-config-f256
| ![](/gif/stylegan2-church-config-f256.gif) | resolution: 256x256 <br/> <br/> author: [NVLabs] (https://www.nvidia.com/en-us/research/) <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# stylegan2-ffhq-config-e1024
| ![](/gif/stylegan2-ffhq-config-e1024.gif) | resolution: 256x256 <br/> <br/> author: [NVLabs] (https://www.nvidia.com/en-us/research/) <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# tryptophobia
| ![](/gif/tryptophobia.gif) | resolution: 256x256 <br/> <br/> author:  <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# ukiyoe-256-slim
| ![](/gif/ukiyoe-256-slim.gif) | resolution: 256x256 <br/> <br/> author:  <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# wildlife
| ![](/gif/wildlife_michael_friesen.gif) | resolution: 256x256 <br/> <br/> author: Michael Friesen <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# paeonia
| ![](/gif/flowers.gif) | resolution: 256x256 <br/> <br/> author: Matthias Grund <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# ffhq_insects
| ![](/gif/ffhq_insects.gif) | resolution: 256x256 <br/> <br/> author: Matthias Grund <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |

# animeportraits
| ![](/gif/animeportraits.gif) | resolution: 256x256 <br/> <br/> author: Michael Friesen <br/> <br/> dataset:  <br/> <br/> license: ... <br/> <br/>  misc.: bla |
| ------ | ------ |






